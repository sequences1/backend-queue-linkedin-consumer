package me.relevante.backend.queue.persistence;

import com.mongodb.MongoClient;
import org.springframework.stereotype.Component;

@Component
public interface MongoConnection {

    MongoClient getMongoClient();
    String getDatabaseName();
}
