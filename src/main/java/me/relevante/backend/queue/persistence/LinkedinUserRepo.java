package me.relevante.backend.queue.persistence;

import me.relevante.linkedin.model.LinkedinUser;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface LinkedinUserRepo extends CrudRepository<LinkedinUser, String> {

    LinkedinUser findOneByLinkedinId(String linkedinId);

    List<LinkedinUser> findByLinkedinIdIn(List<String> linkedinIds);

    LinkedinUser save(LinkedinUser linkedinUser);
    
}