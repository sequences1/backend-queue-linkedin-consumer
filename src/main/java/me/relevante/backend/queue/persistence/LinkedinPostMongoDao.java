package me.relevante.backend.queue.persistence;

import com.mongodb.BasicDBObject;
import me.relevante.linkedin.model.LinkedinPost;
import me.relevante.model.Keyword;
import org.bson.types.ObjectId;
import org.mongodb.morphia.Key;
import org.mongodb.morphia.Morphia;
import org.mongodb.morphia.aggregation.Accumulator;
import org.mongodb.morphia.aggregation.Sort;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.dao.BasicDAO;
import org.mongodb.morphia.query.Criteria;
import org.mongodb.morphia.query.Query;
import org.mongodb.morphia.query.UpdateOperations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import static org.mongodb.morphia.aggregation.Group.grouping;

@Repository
public class LinkedinPostMongoDao extends BasicDAO<LinkedinPost, ObjectId> {

	private UpdateOperations<LinkedinPost> ops;

    @Autowired
    public LinkedinPostMongoDao(MongoConnection mongoConnection) {
        super(mongoConnection.getMongoClient(), new Morphia().map(LinkedinPost.class), mongoConnection.getDatabaseName());
    }
	
	public void deleteByGroupId(String groupId) {
		Query<LinkedinPost> q = getDs().createQuery(LinkedinPost.class).
				filter("groupId",groupId);
		getDs().delete(q);
	}
	
	public List<LinkedinPost> findAll() {
		Query<LinkedinPost> q = getDs().find(LinkedinPost.class);
		return q.asList();
	}

	public List<LinkedinPost> findByGroup(String groupId){
		Query<LinkedinPost> q = getDs().find(LinkedinPost.class,"groupId",groupId);
		return q.asList();
	}

    public LinkedinPost findByPostId(String postId){
        Query<LinkedinPost> q = getDs().find(LinkedinPost.class,"postId",postId);
        if (q.iterator().hasNext())
            return q.iterator().next();
        return null;
    }

    public List<LinkedinPost> findByStem(List<String> stems) {
		
		Query<LinkedinPost> query = getDs().createQuery(LinkedinPost.class);
        List<Criteria> criterias = new LinkedList<>();
		for (String stem: stems){
            criterias.add(query.criteria("stems.stem").equal(stem));
		}
        query.or(criterias.toArray(new Criteria[]{}));
        return query.asList();
	}

	public List<LinkedinPost> findByUser(String userId){
		Query<LinkedinPost> q =
				getDs().find(LinkedinPost.class,"userId",userId);
		return q.asList();
		
	}

	public void updateDocumentKeywordsByPostId(String _id, List<String> keywords){
		Query<LinkedinPost> updateQuery =
				getDs().createQuery(LinkedinPost.class).field("postId").equal(_id);

		ops = getDs().createUpdateOperations(LinkedinPost.class).set("keywords", keywords);
		getDs().update(updateQuery, ops);
	}

	public void updateDocumentStemsByPostId(String _id, List<Keyword> listKeywords) {
		
		Query<LinkedinPost> updateQuery =
				getDs().createQuery(LinkedinPost.class).field("postId").equal(_id);

		ops = getDs().createUpdateOperations(LinkedinPost.class).set("stems", listKeywords);
		getDs().update(updateQuery, ops);
		
	}

    public List<LinkedinPost> getListByUserId(String userId) {

        Query<LinkedinPost> postsQuery = getDs().find(LinkedinPost.class, "userId", userId);
        List<LinkedinPost> linkedinPosts = getListFromIterator(postsQuery.iterator());

        return linkedinPosts;
    }

    public List<LinkedinPost> getListByUserIdSortedByDate(String userId,
                                                          int maxPosts) {

        Query<LinkedinPost> postsQuery = getDs()
                .find(LinkedinPost.class, "userId", userId, 0, maxPosts)
                .order("-date");
        List<LinkedinPost> linkedinPosts = getListFromIterator(postsQuery.iterator());

        return linkedinPosts;
    }

    @Entity
    private static class UserResult {

        @Id
        private String _id;
        private String maxDate;

        public String getUserId() {
            return _id;
        }

        public String getMaxDate() {
            return maxDate;
        }
    }

    public List<String> getUserIdsSortedByLastPostDate(List<String> userIds) {

        Iterator<UserResult> iterator = getDs().createAggregation(LinkedinPost.class)
                .match(createQuery().field("userId").in(userIds))
                .group("userId", grouping("maxDate", new Accumulator("$max", "date")))
                .sort(new Sort("maxDate", -1))
                .aggregate(UserResult.class);

        List<String> sortedUserIds = new ArrayList<>();
        while (iterator.hasNext()) {
            UserResult userResult = iterator.next();
            sortedUserIds.add(userResult.getUserId());
        }

        return sortedUserIds;
    }

    public List<LinkedinPost> getPostsWithLikesAndComments(int maxPosts) {
        BasicDBObject criteria = new BasicDBObject("likes.1", new BasicDBObject("$exists", true));
        criteria.append("comments.1", new BasicDBObject("$exists", true));
        Query<LinkedinPost> query = getDs().createQuery(LinkedinPost.class, criteria)
                .limit(maxPosts);
        List<LinkedinPost> posts = getListFromIterator(query.iterator());

        return posts;
    }

	@Override
	public Key<LinkedinPost> save(LinkedinPost document){
			LinkedinPost d =
					getDs()
					.find(LinkedinPost.class,"postId",document.getPostId())
					.limit(1)
					.get();
			if (d != null){
				document.setId(d.getId());
			}
		return super.save(document);
		
	}

    private List<LinkedinPost> getListFromIterator(Iterator<LinkedinPost> iterator) {
        List<LinkedinPost> linkedinPosts = new ArrayList<>();
        while (iterator.hasNext()) {
            LinkedinPost linkedinPost = iterator.next();
            linkedinPosts.add(linkedinPost);
        }
        return linkedinPosts;
    }
}
