package me.relevante.backend.queue.persistence;

import me.relevante.model.Network;
import me.relevante.model.RelatedUser;
import me.relevante.model.RelatedUserId;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RelatedUserRepo extends CrudRepository<RelatedUser, RelatedUserId> {

    List<RelatedUser> findByRelevanteIdAndNetwork(long relevanteId, Network network);

    RelatedUser findOneByRelevanteId(long relevanteId);

    RelatedUser findOneByRelevanteIdAndUserId(long relevanteId, String userId);

    List<RelatedUser> findByRelevanteId(long relevanteId);

    RelatedUser findOneByRelevanteIdAndUserIdAndNetwork(long relevanteId, String userId, Network network);

    void deleteByRelevanteIdAndNetwork(long relevanteId, Network network);

    void deleteByRelevanteIdAndUserId(long relevanteId, String userId);

    void deleteByRelevanteIdAndUserIdAndNetwork(long relevanteId, String userId, Network network);
}
