package me.relevante.backend.queue.consumer;

import me.relevante.model.oauth.OAuthKeyPair;
import me.relevante.nlp.Classifier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class LinkedinBaseConsumer extends BaseConsumer {

    @Autowired
    protected Classifier classifier;
    @Autowired
    protected OAuthKeyPair oAuthConsumerKeyPair;

}