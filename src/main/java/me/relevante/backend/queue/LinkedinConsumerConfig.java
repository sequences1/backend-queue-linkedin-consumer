package me.relevante.backend.queue;

import me.relevante.backend.queue.consumer.BaseConsumerConfig;
import me.relevante.model.oauth.OAuthKeyPair;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("me.relevante")
public class LinkedinConsumerConfig extends BaseConsumerConfig {

    @Value("${linkedIn.apiKey}")
    private String apiConsumerKey;
    @Value("${linkedIn.apiSecret}")
    private String apiConsumerSecret;

    @Bean
    public OAuthKeyPair linkedinConsumerKeyPair() {
        OAuthKeyPair oAuthKeyPair = new OAuthKeyPair(apiConsumerKey, apiConsumerSecret);
        return oAuthKeyPair;
    }

}
